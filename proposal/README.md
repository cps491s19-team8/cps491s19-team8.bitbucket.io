University of Dayton

Department of Computer Science

CPS 491 - Spring 2019

Dr. Phu Phung

# Capstone II Project #

### HTML5-based Property Sketch Web Application ###

### Team 8 members ###

1. Caroline Gallo, galloc3@udayton.edu
2. Daniel Illg, illgd1@udayton.edu
3. Michael Carlotti, carlottim1@udayton.edu

### Company Mentors ###

Dwayne Nickels, Architect

Chris Drake, Project Manager

Michael Lange, VP of Development

Tyler Technologies

1 Tyler Way

Moraine OH 45439

### Project Management Information

Management board (private access) https://trello.com/b/L9bTqRTW/cps491-scrumteam3

Source code repository (private access) https://bitbucket.org/cps491s19-team8/cps491s19-team8.bitbucket.io/src/master/

Project homepage (public) https://cps491s19-team8.bitbucket.io

### Implementation ###

### Deployment ###

### Software Process Management ###

### Scrum Process ###
#### Sprint 0 ####
Duration: 1/15/2019-1/28/2019
####  Completed Tasks ####
#### Contributions ####
#### Sprint Retrospection ####

### User guide/Demo ###


### Acknowledgments ###


### Overview ###

![Architecture](https://trello-attachments.s3.amazonaws.com/5bedac42ca1a0053c3bf4b44/5bee069a5e2c2436021f5cc8/db89ac738f1e8f9dc7036fdcdf7088e5/Tech_Tech2.0.png) 

### Project Context and Scope ###

For this project, we will be attempting to improve an existing sketch application in HTML5. The current application is used to sketch and calculate the area of existing houses. The application is used to estimate property values in neighborhoods by having workers go out and sketch these houses in the application.  This information is stored and updated every few years. As for the scope of this project, our team will only be dealing with the sketching functionality and with the calculating of the area and perimeter of sketched polygons.

### High-level Requirements ###

- Sketch application can read sketch files from an in-memory data structure.  The format of the sketch files will need to be determined.
- Sketch application can write representations of sketches to an in-memory data structure.
- There will need to be some way for a human to “load” the in-memory data structure with a sketch file.
- Sketch application will automatically zoom to a zoom level that allows all polygons to be viewable on the canvas when a sketch is first rendered.
- Sketch application can read a pre-existing sketch vector and render the polygon with correct dimensions and in correct position
- Users can zoom in/out.
- Users can pan around the sketch canvas.
- Users can edit an existing polygon and change the dimensions and placement of the polygon
- Users can create new polygons
- The origin point of any new polygon can be anywhere on the canvas
- Users can draw polygons of n sides
- By default, polygons are drawn with 90 deg. angles and all lines are “straight” (ie. it’s not free-form mouse draw)
- Users can draw polygons with non-90 deg. angled lines (ie. diagonal lines) and curves (ie. “bows” in iSketch terminology)
- Polygons must “finish” in order to be accepted/persisted
- On completing a polygon, the sketch application will calculate an area and perimeter for the polygon
- Users can assign one and only one “code” to a polygon; users do not have to assign a “code”
- The sketch application opens natively in a browser and requires no additional plug-ins, apps, etc. to be downloaded on a client’s workstation
- An “auto-finish” option to automatically close an open polygon 

### Technology ###

For this project, we will be using HTML5. It is our goal to have the program run natively in the browser without the need to download additional technologies. We will not be dealing with any databases as this part of the application already exists. We will be working with file input and output on our end of this project. Programmers at Tyler Technologies will change this input and output to suit their needs later.  

### Project Management ###

Our plan is to have two week sprint cycles next semester. We plan on having weekly meetings next semester which will be determined once we know our schedules in addition to meeting twice a week for class. We will also meet at least once a week this semester to work on our project analysis and design due at the end of the semester.

Bitbucket: https://bitbucket.org/cps491scrumteam3/src/src

Trello: https://trello.com/b/L9bTqRTW/cps491-scrumteam3

### Company Support ###

The company will support us in Fall 2018 by providing us with the requirements of the project and giving us their expections. They will support us in Spring 2019 by helping us with any questions or issues we may run into as we are working on the project. The communication plan with the company is to have an open conversation through email and to alternate face-to-face and video meetings at the end of each sprint.
